CC	:= g++

CFLAGS = -std=c++17 $(CFLAGS.$@)
GFLAGS := -lgcov -fprofile-arcs -ftest-coverage

TRASH := *.gcda *.gcov *.gcno *.o

CFLAGS.test := $(GFLAGS)
test: test.o BitArray.o
	@echo "Linking test executable..."
	$(CC) $^ -o $@ $(CFLAGS)
	@echo "Done"

run: test
	@echo "Starting test\n"
	@echo "Valgrind check"
	@valgrind ./test 1>/dev/null || echo "\n"
	@echo "Tests check"
	@./test
	@echo "Coverage test"
	@gcov BitArray.gcda | grep BitArray.cpp -A3 -m1

test.o: test.cpp BitArray.h
	@echo "Making test.o..."
	$(CC) -c $< -o $@ $(CFLAGS)
	@echo "Done"

CFLAGS.BitArray.o := $(GFLAGS)
BitArray.o: BitArray.cpp BitArray.h
	@echo "Making BitArray.o..."
	$(CC) -c $< -o $@ $(CFLAGS)
	@echo "Done"

clean:
	@$(RM) $(TRASH) test

