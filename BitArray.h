#include <cstdint>
#include <cstddef>
#include <iterator>

#define NBIT 64

class BitArray
{
private:
    std::uint64_t   *data_  = nullptr;
    std::size_t     nfield_ = 0;
    std::uint8_t    lfnbit_ = 0;

    class Proxy;

public:
    ~BitArray();

    BitArray();
    BitArray(const std::size_t size, bool filler = false);
    BitArray(const BitArray& that);
    BitArray(BitArray&& that);

    Proxy       operator[](int index);
    const Proxy operator[](int index) const;

    BitArray& operator=(const BitArray& that);
    BitArray& operator=(BitArray&& that);

    int find(int start, int end, bool val) const;

    class Iterator;

    Iterator begin();
    Iterator rbegin();
    Iterator end();
    Iterator rend();

    std::size_t size() const;
    std::size_t resize(std::size_t nsize, bool force = false);

    friend bool operator==(const BitArray& a, const BitArray& b);
    friend bool operator!=(const BitArray& a, const BitArray& b);

    friend BitArray operator<<(const BitArray& a, int shift);
    friend BitArray operator>>(const BitArray& a, int shift);

    friend BitArray operator&(const BitArray& a, const BitArray& b);
    friend BitArray operator|(const BitArray& a, const BitArray& b);
    friend BitArray operator^(const BitArray& a, const BitArray& b);
    friend BitArray operator~(const BitArray& a);
};

class BitArray::Proxy
{
    friend class BitArray::Iterator;
private:
    std::uint8_t    shift_ = 0;
    std::uint64_t   *ptr_  = nullptr;

public:
    ~Proxy()    = default;
    Proxy()     = default;

    Proxy(std::uint8_t shift, std::uint64_t *ptr);
    Proxy(const Proxy& that) = default;

    Proxy& operator=(const Proxy &that);
    Proxy& operator=(bool value);
    operator bool() const;

};

class BitArray::Iterator: public std::iterator<
                                    std::random_access_iterator_tag,
                                    Proxy,
                                    long,
                                    Proxy*,
                                    Proxy&>
{
    friend class BitArray;
private:
    bool        reverse_    = false;
    uint64_t*   data_       = nullptr;
    BitArray*   parent_     = nullptr;
    int64_t     index_      = 0;

    BitArray::Proxy proxy_;

    bool        comp(const Iterator& that) const;
    bool        check() const;

    Iterator(BitArray *parent, int64_t index_, bool reverse = false);

public:
    ~Iterator() = default;
    Iterator() = delete;

    Iterator(const Iterator& that) = default;
    Iterator& operator=(const Iterator& that) = default;

    Iterator operator++(int);
    Iterator operator--(int);
    Iterator& operator++();
    Iterator& operator--();

    friend Iterator operator+(const Iterator& it, int shift);
    friend Iterator operator+(int shift, const Iterator& it);
    friend Iterator operator-(const Iterator& it, int shift);

    friend bool operator==(const Iterator& a, const Iterator& b);
    friend bool operator!=(const Iterator& a, const Iterator& b);
    friend bool operator>=(const Iterator& a, const Iterator& b);
    friend bool operator<=(const Iterator& a, const Iterator& b);
    friend bool operator>(const Iterator& a, const Iterator& b);
    friend bool operator<(const Iterator& a, const Iterator& b);

    Iterator& operator+=(int shift);
    Iterator& operator-=(int shift);

    friend long operator-(const Iterator& a, const Iterator& b);

    Proxy& operator[](int index);
    Proxy& operator*();
};
