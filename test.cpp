#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <cstdlib>
#include "BitArray.h"
#include <stdexcept>
#include <exception>

#define DESTRUCT_AT_MEM         ((BitArray*)mem)->~BitArray()
#define CONSTRUCT_AT_MEM(sz...) new (mem) BitArray(sz)
#define ARRAY_AT_MEM            (*((BitArray*)mem))  

bool INJECT_FAULT = false;

void *operator new(size_t size)
{
    if(INJECT_FAULT)
        throw std::bad_alloc();

    void* result = malloc(size);
    if(result == NULL)
        throw std::bad_alloc();

    return result;
}

SCENARIO("BitArray can be constructed and destructed")
{
    GIVEN("Space to costruct")
    {
        void *mem = malloc(sizeof(BitArray));
        REQUIRE(mem != nullptr);

        WHEN("Default constructor called")
        {
            THEN("No exceptions are generated")
            {
                CHECK_NOTHROW(CONSTRUCT_AT_MEM());
                REQUIRE(ARRAY_AT_MEM.size() == 0);
                DESTRUCT_AT_MEM;
            }
        }

        WHEN("Constructor of certain size is called")
        {
            WHEN("No alloc errors rises")
            {
                THEN("No exceptions are generated")
                {
                    CHECK_NOTHROW(CONSTRUCT_AT_MEM(10));
                    CHECK_NOTHROW(DESTRUCT_AT_MEM);

                    CHECK_NOTHROW(CONSTRUCT_AT_MEM(10, true));
                    CHECK_NOTHROW(DESTRUCT_AT_MEM);
                }

                THEN("Requested size matches with result")
                {
                    CHECK_NOTHROW(CONSTRUCT_AT_MEM(10));
                    REQUIRE(ARRAY_AT_MEM.size() == 10);
                    CHECK_NOTHROW(DESTRUCT_AT_MEM);

                    CHECK_NOTHROW(CONSTRUCT_AT_MEM(8));
                    REQUIRE(ARRAY_AT_MEM.size() == 8);
                    CHECK_NOTHROW(DESTRUCT_AT_MEM);

                    CHECK_NOTHROW(CONSTRUCT_AT_MEM(0));
                    REQUIRE(ARRAY_AT_MEM.size() == 0);
                    CHECK_NOTHROW(DESTRUCT_AT_MEM);

                    CHECK_NOTHROW(CONSTRUCT_AT_MEM(7));
                    REQUIRE(ARRAY_AT_MEM.size() == 7);
                    CHECK_NOTHROW(DESTRUCT_AT_MEM);
                }

                WHEN("filler is specified")
                {
                    THEN("Array is true-filled")
                    {
                        CHECK_NOTHROW(CONSTRUCT_AT_MEM(10, true));
                        REQUIRE((bool)ARRAY_AT_MEM[7] == true);
                        CHECK_NOTHROW(DESTRUCT_AT_MEM);
                    }
                }

            }
            WHEN("Alloc error rises")
            {
                THEN("Bad alloc is thrown")
                {
                    INJECT_FAULT = true;

                    CHECK_THROWS_AS(new (mem) BitArray(10), 
                            std::bad_alloc);
                    CHECK_NOTHROW(DESTRUCT_AT_MEM);
                    INJECT_FAULT = false;
                }
            }
        }

        GIVEN("dummy array to copy")
        {
            WHEN("Copy constructor is called")
            {
                BitArray dummy(10, true);
                WHEN("No alloc error rises")
                {
                    THEN("No exceptions are thrown")
                    {
                        CHECK_NOTHROW(CONSTRUCT_AT_MEM(dummy));
                        CHECK_NOTHROW(DESTRUCT_AT_MEM);
                    }
                }
                WHEN("Alloc error rises")
                {
                    THEN("bad_alloc is thrown")
                    {
                        INJECT_FAULT = true;

                        CHECK_THROWS_AS(CONSTRUCT_AT_MEM(dummy), 
                                std::bad_alloc);
                        CHECK_NOTHROW(DESTRUCT_AT_MEM);

                        INJECT_FAULT = false;
                    }
                }
            }
            WHEN("Move constructor is called")
            {
                THEN("No errors should be there")
                {
                    BitArray dummy(10, true);
                    CHECK_NOTHROW(CONSTRUCT_AT_MEM(std::move(dummy)));
                    CHECK_NOTHROW(DESTRUCT_AT_MEM);
                }
            }
        }

        free(mem);
    }
}

#undef CONSTRUCT_AT_MEM
#undef DESTRUCT_AT_MEM
#undef ARRAY_AT_MEM

SCENARIO("BitArray can be accesed via operator[]")
{
    GIVEN("dummy with size 10 to access")
    {
        BitArray dummy(10, true);

        WHEN("Everything is ok")
        {
            THEN("Everything is ok")
            {
                REQUIRE(dummy[0] == true);
                REQUIRE(dummy[9] == true);
                REQUIRE_FALSE((bool)(dummy[7] = false));
                REQUIRE(dummy[7] == false);
                REQUIRE((bool)(dummy[8] = true));
                REQUIRE((bool)(dummy[7] = dummy[8]));
            }
        }

        WHEN("Access is out-of-range")
        {
            THEN("out_of_range is thrown")
            {
                CHECK_THROWS_AS(dummy[-1], std::out_of_range);
                CHECK_THROWS_AS(dummy[-15], std::out_of_range);
                CHECK_THROWS_AS(dummy[10], std::out_of_range);
                CHECK_THROWS_AS(dummy[15], std::out_of_range);
            }
        }

        WHEN("dummy is const")
        {
            const BitArray cdummy(10, true);
            THEN("it also works")
            {
                WHEN("Everything is ok")
                {
                    THEN("Everything is ok")
                    {
                        REQUIRE(cdummy[0] == true);
                        REQUIRE(cdummy[9] == true);
                        REQUIRE(cdummy[7] == true);
                    }
                }

                WHEN("Access is out-of-range")
                {
                    THEN("out_of_range is thrown")
                    {
                        CHECK_THROWS_AS(cdummy[-1], std::out_of_range);
                        CHECK_THROWS_AS(cdummy[-15], std::out_of_range);
                        CHECK_THROWS_AS(cdummy[10], std::out_of_range);
                        CHECK_THROWS_AS(cdummy[15], std::out_of_range);
                    }
                }
            }
        }
    }
}

SCENARIO("BitArray can be assigned")
{
    WHEN("Self assignment")
    {
        BitArray dummy1(10, true);
        THEN("Nothing happens")
        {
            INJECT_FAULT = true;
            CHECK_NOTHROW(dummy1 = dummy1);
            INJECT_FAULT = false;
        }
    }

    GIVEN("2 originally different dummies")
    {
        BitArray dummy1(10, true);
        BitArray dummy2(15, false);

        WHEN("Simple operator is called")
        {
            WHEN("No alloc error rises")
            {
                THEN("They are identical after")
                {
                    dummy1 = dummy2;
                    REQUIRE(dummy1.size() == dummy2.size());
                    REQUIRE(dummy1[3] == dummy2[3]);
                }
            }
            WHEN("Alloc error rises")
            {
                THEN("nothing changes, only exception is thrown")
                {
                    INJECT_FAULT = true;

                    REQUIRE(dummy1.size() != dummy2.size());
                    CHECK_THROWS_AS(dummy1 = dummy2, std::bad_alloc);

                    REQUIRE(dummy1.size() != dummy2.size());
                    REQUIRE(dummy1[3] != dummy2[1]);

                    INJECT_FAULT = false;
                }
            }
        }

        WHEN("move operator is called")
        {
            THEN("No memory allocations")
            {
                BitArray dummy(10);

                INJECT_FAULT = true;

                CHECK_NOTHROW(dummy = BitArray(0));

                INJECT_FAULT = false;
            }
        }
    }
}

SCENARIO("BitArray can be resized")
{
    GIVEN("dummy to resize")
    {
        BitArray dummy(10);

        WHEN("No alloc errors rises")
        {
            WHEN("resize is forced")
            {
                WHEN("it causes data loss")
                {
                    THEN("Well, it's forced")
                    {
                        CHECK_NOTHROW(dummy.resize(10));
                        CHECK_NOTHROW(dummy.resize(8, true));
                        REQUIRE(dummy.size() == 8);
                        CHECK_NOTHROW(dummy.resize(0, true));
                        CHECK_NOTHROW(dummy.resize(64, false));
                        REQUIRE(dummy.size() == 64);
                    }
                }
            }
            WHEN("resize is not forced")
            {
                THEN("Throws length_error exception")
                {
                    CHECK_THROWS_AS(dummy.resize(9), std::length_error);
                    REQUIRE(dummy.size() == 10);
                }
            }
        }

        WHEN("allocation is bad")
        {
            THEN("bad_alloc is thrown")
            {
                INJECT_FAULT = true;
                CHECK_THROWS_AS(dummy.resize(100), std::bad_alloc);
                CHECK_NOTHROW(dummy.resize(0, true));
                INJECT_FAULT = false;
            }
        }
    }
}

SCENARIO("BitArray is iterable is STL-style")
{
    GIVEN("dummy aray to make iterations")
    {
        BitArray dummy(10);
        auto it1 = dummy.begin();
        auto it2 = dummy.end();

        auto rit1 = dummy.rbegin();
        auto rit2 = dummy.rend();

        WHEN("Everything is ok")
        {
            THEN("We can make all the operations with it")
            {
                REQUIRE(it1 < it2);
                REQUIRE(it1 <= it2);
                REQUIRE(it1 <= it1);
                REQUIRE(it1 == it1);
                REQUIRE(it2 >= it1);
                REQUIRE(it2 >= it2);
                REQUIRE(it2 > it1);
                REQUIRE(it1 != it2);
                REQUIRE(it2 - it1 == dummy.size());

                CHECK_NOTHROW(it1++);
                CHECK_NOTHROW(it1--);
                CHECK_NOTHROW(--it1);
                CHECK_NOTHROW(++it1);
                CHECK_NOTHROW(it1 + 10);
                CHECK_NOTHROW(10 + it1);
                CHECK_NOTHROW(it1 - 10);
                CHECK_NOTHROW(it1 += 10);
                CHECK_NOTHROW(it1 -= 10);

                REQUIRE(rit1 < rit2);
                REQUIRE(rit1 <= rit2);
                REQUIRE(rit1 <= rit1);
                REQUIRE(rit1 == rit1);
                REQUIRE(rit2 >= rit1);
                REQUIRE(rit2 >= rit2);
                REQUIRE(rit2 > rit1);
                REQUIRE(rit1 != rit2);
                REQUIRE(rit2 - rit1 == dummy.size());
                
                CHECK_NOTHROW(rit1++);
                CHECK_NOTHROW(rit1--);
                CHECK_NOTHROW(--rit1);
                CHECK_NOTHROW(++rit1);
                CHECK_NOTHROW(rit1 + 10);
                CHECK_NOTHROW(10 + rit1);
                CHECK_NOTHROW(rit1 - 10);
                CHECK_NOTHROW(rit1 += 10);
                CHECK_NOTHROW(rit1 -= 10);
            }
        }

        WHEN("Mixing different iterators")
        {
            WHEN("different directions")
            {
                THEN("Exception is generated")
                {
                    CHECK_THROWS(rit2 <= it1);
                    CHECK_THROWS(rit2 == it1);
                    
                    CHECK_THROWS(rit1 - it1 != 0);
                }
            }
            WHEN("different arrays")
            {
                THEN("It is always false")
                {
                    BitArray dummy2(15);
                    auto it5 = dummy2.begin();
                    REQUIRE_FALSE(it1 < it5);
                    REQUIRE_FALSE(it1 == it5);

                    CHECK_THROWS(rit1 - it5 != 0);
                }
            }
            WHEN("Iterator is deprecated")
            {
                BitArray dummy2(15);
                auto it5 = dummy2.begin();
                auto it6 = dummy2.end();
                dummy2.resize(20);

                THEN("Exception is generated")
                {
                    CHECK_THROWS(it6 - it5);
                }
            }
        }

        WHEN("Dereferencing iterators")
        {
            WHEN("Everything is okay")
            {
                THEN("it works")
                {
                    CHECK_NOTHROW(*it1);
                    CHECK_NOTHROW(it1[2]);
                }
            }
            WHEN("Iterator is out-of-range")
            {
                THEN("Exception is generated")
                {
                    it1 += 1000;
                    CHECK_THROWS(*it1);
                    CHECK_THROWS(it1[0]);
                }
            }
        }
    }
}

SCENARIO("BitArray support some bitwise operators")
{
    GIVEN("Two compatible arrays")
    {
        BitArray dummy1(68), dummy2(68, true);

        CHECK_NOTHROW(dummy1 == dummy2);
        REQUIRE_FALSE(dummy1 == dummy2);
        REQUIRE(dummy1 != dummy2);

        CHECK_NOTHROW(~dummy1);
        CHECK_NOTHROW(dummy1 & dummy2);
        CHECK_NOTHROW(dummy1 | dummy2);
        CHECK_NOTHROW(dummy1 ^ dummy2);
    }

    GIVEN("Two incompatible arrays")
    {
        BitArray dummy1(10), dummy2(11, true);

        CHECK_THROWS(dummy1 == dummy2);
        CHECK_THROWS(dummy1 == dummy2);
        CHECK_THROWS(dummy1 != dummy2);

        CHECK_THROWS(dummy1 & dummy2);
        CHECK_THROWS(dummy1 | dummy2);
        CHECK_THROWS(dummy1 ^ dummy2);
    }

    GIVEN("Array to shift")
    {
        BitArray a(10, true);
        CHECK_NOTHROW(a);
        CHECK_NOTHROW(a << 1);
        CHECK_NOTHROW(a << 5);
        CHECK_NOTHROW(a << 10);
        CHECK_NOTHROW(a >> 1);
        CHECK_NOTHROW(a >> 5);
        CHECK_NOTHROW(a >> 15);
    }
}

SCENARIO("bitfind")
{
    GIVEN("BitArray to find bits")
    {

        WHEN("out_of_range")
        {
            THEN("Exception is thrown")
            {
                BitArray dummy(288);
                
                CHECK_THROWS(dummy.find(-1, 280, true));
                CHECK_THROWS(dummy.find(0, 290, true));
                CHECK_THROWS(dummy.find(-1, 290, true));

                CHECK_THROWS(dummy.find(-1, 280, false));
                CHECK_THROWS(dummy.find(0, 290, false));
                CHECK_THROWS(dummy.find(-1, 290, false));
            }
        }

        WHEN("1 is searched")
        {
            WHEN("both ends in same field")
            {
                WHEN("bit in middle of field")
                {
                    BitArray dummy(288);
                    dummy[69] = 1;
                    REQUIRE(dummy.find(64, 127, true) == 69);
                    REQUIRE(dummy.find(66, 125, true) == 69);
                    REQUIRE(dummy.find(66, 69, true) == -1);
                    REQUIRE(dummy.find(69, 120, true) == 69);
                }
                WHEN("bit on the end of field")
                {
                    BitArray dummy(288);

                    dummy[64] = 1;

                    REQUIRE(dummy.find(64, 127, true) == 64);
                    REQUIRE(dummy.find(67, 127, true) == -1);

                    dummy[64] = 0;
                    dummy[127] = 1;

                    REQUIRE(dummy.find(64, 127, true) == -1);
                    REQUIRE(dummy.find(64, 125, true) == -1);
                }

                WHEN("Corner situation")
                {
                    BitArray d(128);
                    d[1] = 1;
                    d[63] = 1;
                    d[50] = 1;
                    REQUIRE(d.find(2, 50, true) == -1);
                    REQUIRE(d.find(2, 65, true) == 50);
                    REQUIRE(d.find(0, 63, true) == 1);
                    REQUIRE(d.find(0, 65, true) == 1);

                    d = BitArray(128);
                    d[1] = 1;
                    d[60] = 1;
                    REQUIRE(d.find(2, 59, true) == -1);
                    REQUIRE(d.find(2, 66, true) == 60);
                    REQUIRE(d.find(2, 61, true) == 60);

                    d = BitArray(128);
                    d[3] = 1;

                    REQUIRE(d.find(4, 128, true) == -1);
                    REQUIRE(d.find(1, 5, true) == 3);

                    d = BitArray(1000);
                    d[3] = 1;
                    d[64 + 15] = 1;
                    d[64 + 16] = 1;
                    REQUIRE(d.find(4, 150, true) == 64 + 15);
                    REQUIRE(d.find(4, 64 + 15, true) == -1);
                }
            }
        }
        WHEN("0 is searched")
        {
            THEN("Everything is same")
            {
                BitArray dummy(288, true);
                dummy[263] = false;

                REQUIRE(dummy.find(0, 288, false) == 263);
            }
        }

    }
}
