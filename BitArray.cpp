#include "BitArray.h"
#include <cstring>
#include <exception>
#include <stdexcept>
#include <utility>
#include <algorithm>
#include <functional>


BitArray::BitArray() {};

BitArray::BitArray(const std::size_t size, bool filler)
{
    size_t nf = 0, lfb = 0;
    if(size % NBIT == 0) {
        nf = size / NBIT;
        lfb = NBIT;
    } else {
        nf = size / NBIT + 1;
        lfb = size % NBIT;
    }
    
    if(nf != 0)
        this->data_ = new uint64_t[nf]{0};
    
    this->nfield_ = nf;
    this->lfnbit_ = lfb;

    if(filler == true)
        memset(this->data_, UINT8_MAX, nfield_ * (NBIT / 8));
}

BitArray::BitArray(const BitArray& that)
{
    this->data_ = new uint64_t[that.nfield_];
    memcpy(this->data_, that.data_, that.nfield_);
    
    this->nfield_ = that.nfield_;
    this->lfnbit_ = that.lfnbit_;
}

BitArray::BitArray(BitArray&& that)
{
    this->data_ = that.data_;
    that.data_ = nullptr;

    this->nfield_ = that.nfield_;
    that.nfield_ = 0;

    this->lfnbit_ = that.lfnbit_;
    that.lfnbit_ = 0;
}

BitArray::~BitArray()
{
    delete[] this->data_;

    this->data_ = nullptr;
    this->nfield_ = 0;
    this->lfnbit_ = 0;
}

BitArray::Proxy::Proxy(std::uint8_t shift, std::uint64_t *ptr)
{
    this->shift_ = shift;
    this->ptr_ = ptr; 
}

BitArray::Proxy& BitArray::Proxy::operator=(const BitArray::Proxy& that)
{
    bool value = (bool)that;
    *this = value;

    return *this;
}

BitArray::Proxy& BitArray::Proxy::operator=(bool value)
{
    if(value)
        *(this->ptr_) = *(this->ptr_) | ((uint64_t)0b1 << this->shift_); 
    else
        *(this->ptr_) = *(this->ptr_) & (~((uint64_t)0b1 << this->shift_));
 
    return *this;
}

BitArray::Proxy::operator bool() const
{
    bool res = *(this->ptr_) & ((uint64_t)0b1 << this->shift_);
    return res;
}

BitArray::Proxy BitArray::operator[](int index)
{
    if(index < 0 || index >= this->size())
        throw std::out_of_range(__func__);

    BitArray::Proxy res(index % NBIT, this->data_ + index / NBIT);

    return res;
}

const BitArray::Proxy BitArray::operator[](int index) const
{
    if(index < 0 || index >= this->size())
        throw std::out_of_range(__func__);

    BitArray::Proxy res(index % NBIT, this->data_ + index / NBIT);

    return res;
}

BitArray& BitArray::operator=(const BitArray& that)
{
    if(this == &that)
        return *this;

    uint64_t *ndata = new uint64_t[that.nfield_];
    memcpy(ndata, that.data_, that.nfield_);

    delete[] this->data_;
    this->data_ = ndata;

    this->nfield_ = that.nfield_;
    this->lfnbit_ = that.lfnbit_;
    
    return *this;
}

BitArray& BitArray::operator=(BitArray&& that)
{
    std::swap(this->data_, that.data_);
    std::swap(this->nfield_, that.nfield_);
    std::swap(this->lfnbit_, that.lfnbit_);

    return *this;
}
 
std::size_t BitArray::size() const
{
    if(this->nfield_ == 0) 
        return 0;
    return ((this->nfield_ - 1) * NBIT ) + (this->lfnbit_);
}

std::size_t BitArray::resize(std::size_t nsize, bool force)
{
    std::size_t osize = this->size();
    if(nsize == osize)
        return osize;

    if(nsize < osize && !force)
        throw std::length_error("Resize will cause loss of data");

    if(nsize == 0)
    {
        delete[] this->data_;
        this->data_ = nullptr;
        this->nfield_ = 0;
        this->lfnbit_ = 0;

        return osize;
    }

    size_t nf = 0, lfb = 0;
    if(nsize % NBIT == 0) {
        nf = nsize / NBIT;
        lfb = NBIT;
    }
    else {
        nf = nsize / NBIT + 1;
        lfb = nsize % NBIT;
    }

    uint64_t *ndata = new uint64_t[nf];
    memcpy(ndata, this->data_, std::min(nf, this->nfield_));
    delete[] this->data_;

    this->data_ = ndata;
    this->nfield_ = nf;
    this->lfnbit_ = lfb;

    return osize;
}

BitArray::Iterator::Iterator(BitArray *parent, int64_t index, bool reverse)
{
    if(parent == nullptr || 
       index < -1 || index > (int64_t)parent->size())
        throw std::invalid_argument("bad construction");
    
    this->parent_   = parent;
    this->reverse_  = reverse;
    this->data_     = parent->data_;
    this->index_    = index;
}

bool BitArray::Iterator::check() const
{
    if(this->data_ != this->parent_->data_)
        throw std::logic_error("Iterator is deprecated");
    return true;
}

bool BitArray::Iterator::comp(const BitArray::Iterator& it) const
{
    this->check();
    it.check();

    return (this->parent_ == it.parent_);
}

BitArray::Iterator BitArray::Iterator::operator++(int)
{
    BitArray::Iterator tmp = *this;
    
    if(this->reverse_)
        this->index_ -= 1;
    else 
        this->index_ += 1;

    return tmp;
}

BitArray::Iterator& BitArray::Iterator::operator++()
{
    if(this->reverse_)
        this->index_ -= 1;
    else 
        this->index_ += 1;

    return *this;
}

BitArray::Iterator BitArray::Iterator::operator--(int)
{
    BitArray::Iterator tmp = *this;
    
    if(this->reverse_)
        this->index_ += 1;
    else 
        this->index_ -= 1;

    return tmp;
}

BitArray::Iterator& BitArray::Iterator::operator--()
{
    if(this->reverse_)
        this->index_ += 1;
    else 
        this->index_ -= 1;

    return *this;
}

BitArray::Iterator& BitArray::Iterator::operator+=(int shift)
{
    if(this->reverse_)
        this->index_ -= shift;
    else
        this->index_ += shift;

    return *this;
}

BitArray::Iterator& BitArray::Iterator::operator-=(int shift)
{
    if(this->reverse_)
        this->index_ += shift;
    else
        this->index_ -= shift;

    return *this;
}

long operator-(const BitArray::Iterator& a, const BitArray::Iterator& b)
{
    a.check();
    b.check();

    if(!a.comp(b) || a.reverse_ != b.reverse_)
        throw std::logic_error("Iterators not compatiple");

    if(a.reverse_)
        return b.index_ - a.index_;
    else
        return a.index_ - b.index_;
}

BitArray::Iterator operator+(const BitArray::Iterator& it, int shift)
{
    BitArray::Iterator tmp = it;
    tmp.index_ += shift;
    return tmp;
}

BitArray::Iterator operator+(int shift, const BitArray::Iterator& it)
{
    return (it + shift);
}

BitArray::Iterator operator-(const BitArray::Iterator& it, int shift)
{
    return (it + (-shift));
}

bool operator==(const BitArray::Iterator& a, const BitArray::Iterator& b)
{
    if(!a.comp(b))
        return false;

    if(a.reverse_ != b.reverse_)
        throw std::logic_error("Iterators not compatible");

    return (a.index_ == b.index_);
}

bool operator<(const BitArray::Iterator& a, const BitArray::Iterator& b)
{
    if(!a.comp(b))
        return false;
    
    if(a.reverse_ != b.reverse_)
        throw std::logic_error("Iterators not compatible");

    if(a.reverse_)
        return (a.index_ > b.index_);
    else
        return (a.index_ < b.index_);
}

bool operator!=(const BitArray::Iterator& a, const BitArray::Iterator& b)
{
    return !(a == b);
}

bool operator>=(const BitArray::Iterator& a, const BitArray::Iterator& b)
{
    return !(a < b);
}

bool operator<=(const BitArray::Iterator& a, const BitArray::Iterator& b)
{
    return (b >= a);
}

bool operator>(const BitArray::Iterator& a, const BitArray::Iterator& b)
{
    return !(a <= b);
}

BitArray::Proxy& BitArray::Iterator::operator*()
{
    check();

    BitArray::Proxy tmp((*(this->parent_))[(int)this->index_]);
    this->proxy_.ptr_   = tmp.ptr_;
    this->proxy_.shift_ = tmp.shift_;

    return this->proxy_;
}

BitArray::Proxy& BitArray::Iterator::operator[](int index)
{
    check();

    BitArray::Proxy tmp((*(this->parent_))[(int)this->index_ + index]);
    this->proxy_.ptr_   = tmp.ptr_;
    this->proxy_.shift_ = tmp.shift_;

    return this->proxy_;
}

BitArray::Iterator BitArray::begin()
{
    return BitArray::Iterator(this, 0);
}

BitArray::Iterator BitArray::end()
{
    return BitArray::Iterator(this, this->size());
}

BitArray::Iterator BitArray::rend()
{
    return BitArray::Iterator(this, -1, true);
}

BitArray::Iterator BitArray::rbegin()
{
    return BitArray::Iterator(this, this->size() - 1, true);
}

BitArray operator<<(const BitArray& a, int shift)
{
    if(shift >= a.size())
       return BitArray(a.size());

    BitArray res = a;

    for(int i = 0; i < a.size() - shift; ++i)
        res[i] = a[i + shift];    

    int size = (int)a.size();
    for(int i = size - shift; i < size; ++i)
        res[i] = false;

    return res;
}

BitArray operator>>(const BitArray& a, int shift)
{
    if(shift >= a.size())
       return BitArray(a.size());

    BitArray res = a;

    for(int i = shift; i < a.size(); ++i)
        res[i] = a[i - shift];

    for(int i = 0; i < shift; ++i)
        res[i] = false;

    return res;
}

bool operator==(const BitArray& a, const BitArray& b)
{
    if(&a == &b)
        return true;
    if(a.size() != b.size())
        throw std::logic_error("Arrays is not compatible");

    std::size_t nf = a.nfield_;
    
    if(a.lfnbit_ != NBIT)
       nf--;

    bool res = true;
    for(int i = 0; res && i < nf; ++i)
        if(a.data_[i] != b.data_[i])
            res = false;

    for(int i = 0; res && i < a.lfnbit_; ++i)
        if(a[i] != b[i])
            res = false;
    
    return res;
}

bool operator!=(const BitArray& a, const BitArray& b)
{
    return !(a == b);
}


BitArray operator&(const BitArray& a, const BitArray& b)
{
    if(a.size() != b.size())
        throw std::logic_error("Arrays is not compatible");

    BitArray res(a.size());

    for(int i = 0; i < a.nfield_; ++i)
        res.data_[i] = a.data_[i] & b.data_[i];

    return res;
}

BitArray operator|(const BitArray& a, const BitArray& b)
{
    if(a.size() != b.size())
        throw std::logic_error("Arrays is not compatible");

    BitArray res(a.size());

    for(int i = 0; i < a.nfield_; ++i)
        res.data_[i] = a.data_[i] | b.data_[i];

    return res;
}

BitArray operator^(const BitArray& a, const BitArray& b)
{
    if(a.size() != b.size())
        throw std::logic_error("Arrays is not compatible");

    BitArray res(a.size());

    for(int i = 0; i < a.nfield_; ++i)
        res.data_[i] = a.data_[i] ^ b.data_[i];

    return res;
}

BitArray operator~(const BitArray& a)
{
    BitArray res(a.size());

    for(int i = 0; i < a.nfield_; ++i)
        res.data_[i] = ~a.data_[i];

    return res;
}

int findbit(int start, int end, uint64_t d, bool val)
{
    if(val) {
        for(int i = start; i < end; ++i)
            if(d & ((uint64_t)0b1 << i))
                return i;
        return -1;
    } else {
        for(int i = start; i < end; ++i)
            if((d & ((uint64_t)0b1 << i)) == 0)
               return i;
       return -1;
    }

    return -1; 
}

int BitArray::find(int start, int end, bool val) const
{
    if(start < 0 || end > this->size() || end <= start)
        throw std::out_of_range(__func__);

    uint64_t *sf = this->data_ + start / NBIT;
    uint64_t *lf = this->data_ + end / NBIT + (int)(end % NBIT != 0);

    int ts = start % NBIT, te = end % NBIT;
    if(start % NBIT != 0) {
        if(sf != lf - 1)
            te = NBIT;
        if(findbit(ts, te, *sf, val) == -1)
            sf++;
    }

    auto p = std::function<bool(uint64_t)>();
    if(val)
        p = [=](uint64_t x) -> bool { return x > 0; };
    else
        p = [=](uint64_t x) -> bool { return x != UINT64_MAX; };

    uint64_t *f = std::find_if(sf, lf, p);
    if(f == lf)
        return -1;

    te = end % NBIT;
    if(f != lf - 1)
        te = NBIT;
    if(f != sf)
        ts = 0;

    int sh = findbit(ts, te, *f, val);
    if(sh == -1)
        return -1;

    return (int)(f - this->data_) * NBIT + sh;
}
